/*
O ficheiro casas.c quando o jogo iniciar, vai criar todas
as casas do jogo:
Casa inicial      -> uma pilha de 52 cartas de ordem aleatoria
Casa monte        -> uma pilha de 52 casas vazias
Casas de passagem -> 4 sequencias de 13 casas vazias
Casas finais      -> 4 pilhas de 13 casas vazias

Tem tb as fun�oes para mover as cartas
*/
#include "pilha.h"
#include "sequencia.h"
#ifndef _H_casas
#define _H_casas
#define MAX_CARTAS_BARALHO 52
#define MAX_CARTAS_FINAL 13
#define MAX_CARTAS_PASSAGEM 13

typedef pilha casa_inicial;
typedef pilha casa_monte;
typedef pilha casa_final;
typedef sequencia casa_passagem;

// Cria uma nova casa_inicial
casa_inicial * casa_inicial_nova();

// Cria uma nova casa_monte
casa_monte * casa_monte_nova();

// Cria uma nova casa_final
casa_final * casa_final_nova();

// Cria uma nova casa_passagem
casa_passagem * casa_passagem_nova();

//move uma carta do monte para uma casa de passagem
void monte_passagem(casa_monte *m, casa_passagem *p);

//move uma carta do monte para uma casa final
void monte_final(casa_monte *m, casa_final *f);

//move uma carta de uma casa de passagem para uma casa final
void passagem_final(casa_passagem *p, casa_final *f);

//move cartas de uma casa de passagem para outra casa de passagem
void passagem_passagem(casa_passagem *f1, casa_passagem *f2);

//determinar o naipe de uma carta
char naipe(int carta);
     
//determinar o numero de uma carta
char numero(int carta);

//determina se uma carta "encaixa" numa casa final; devolve 1 se for possivel e 0 se nao for
int valido_final(int carta, casa_final *f);

//determina se uma carta "encaixa" numa casa de passagem; devolve 1 se for possivel e 0 se nao for
int valido_passagem(int carta, casa_passagem *p);

//determina se � possivel passar cartas de uma sequenci apra outra
//devolve o numero de cartas que pode passar, ou 0 se nao for possivel
int valido_pass_pass(casa_passagem *p1, casa_passagem *p2);

//faz print de uma carta
void print_carta(int carta);

//faz print das cartas de uma pilha
void print_final(casa_final *f);


#endif
