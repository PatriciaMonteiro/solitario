#ifndef _H_pilha
#define _H_pilha

typedef struct elemento_{
   void *dados;//pode ser de qq tipo
   struct elemento_ * proximo;
} Elemento;

typedef struct pilhas_{
   Elemento * topo;
   int max;//tamanho maximo
   int size;//tamanho actual
}pilha;

// Cria uma nova pilha 
pilha * pilha_nova(int max);

// Retorna 1 se a pilha esta vazia e 0 caso contrario 
int pilha_vazia(pilha *p);

// Empilha um dado  
void push(pilha *p, void * dados);

// retorna o topo da pilha.
void * pop(pilha *p);

// Destroi a pilha, e o seu conteudo 
int pilha_destroi(pilha *p);

#endif
