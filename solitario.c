#include <stdio.h>
#include <windows.h>
#include <string.h>
#include "pilha.h"
#include "sequencia.h"
#include "casas.h"

   casa_inicial *inicial;
   casa_monte *monte;  
   casa_passagem *passagem1;
   casa_passagem *passagem2;
   casa_passagem *passagem3;
   casa_passagem *passagem4;
   casa_final *final1;
   casa_final *final2;
   casa_final *final3;
   casa_final *final4;
   int carta;
   int registo[3][6];//guarda as ultimas 6 jogadas entre casas de passagem em registo

int main(){
    int i,j;
    int tirar=0;
    int automatico=0;
    int jogou_auto;
    int fim=0;
    char c=0;
    int escolha;//1 - novo jogo; 2 - carregar jogo
    
    
    while(1)   //ciclo principal do programa
    {
               
        //iniciar o registo, para nao ter valores iguais ao principio
        for(i=0 ; i<6 ; i++)
            for(j=0 ; j<3 ; j++)
                registo[j][i]=100+i;
               
        limpar_ecra();
        escolha=menu();
        limpar_ultima_linha();
        automatico=0;
        
        if (escolha==1)
           novo_jogo();
        if (escolha==2)
           carregar_jogo();
      
        refresh();
        
        //manual ou auto...
        printf("  1 - Modo manual");
        printf("\n  2 - Modo semi-automatico");
        c='0';
        while(c!='1' && c!='2')
            c = getch();
        if (c=='2')
            automatico=1; 
    
        //jogar...
        fim=0;
        while(fim==0)//ciclo de cada jogada
        {
            jogou_auto=1;
            limpar_ultima_linha();
            if (automatico==1)
                while (jogou_auto==1)
                {
                    jogou_auto=jogar_automatico();// se devolver 1, verifica outra vez se ha jogadas
                    if (jogou_auto==1)   
                       printf("\n\nJogada automatica efectuada...");
                }         
            refresh();//actualiza o ecra
            fim=fim_jogo();     //detecta se o jogo acabou
            if (fim==0)     
            {
                printf("Qual a jogada?   (ESC -> Sair)\n");
                tirar=0;
                while (tirar==0)    //so avan�a quando jogar devolver 1 ou 2 (sair)  
                    tirar=jogar();  //faz a jogada
                if (tirar==2)    //se carregar no ESC
                    fim=sair();
            }
        }
    }
}

//inicia um novo jogo
int novo_jogo(){
   inicial = casa_inicial_nova();//iniciar as casas
   monte = casa_monte_nova();
   passagem1 = casa_passagem_nova();
   passagem2 = casa_passagem_nova();
   passagem3 = casa_passagem_nova();
   passagem4 = casa_passagem_nova();
   final1 = casa_final_nova();
   final2 = casa_final_nova();
   final3 = casa_final_nova();
   final4 = casa_final_nova();
}

int jogar(){
    char c;
 
    c = getch();
    limpar_ultima_linha();
    printf("%c ", c);
 
    if (c==27)   //tecla ESC
        return 2;
    
    if (c=='0')//tirar carta da casa inicial    
    {
       if (!pilha_vazia(inicial))//so e possivel se a casa nao estiver vazia
           carta=pop(inicial); 
       else 
       {  
           printf("(Casa vazia!)");  
           return 0;
       }
    }
    else if (c=='1')//tirar carta da casa monte
    {
       if (!pilha_vazia(monte))//so e possivel se a casa nao estiver vazia
           carta=pop(monte); 
       else 
       {  
           printf("(Casa vazia!)");  
           return 0;
       }
    }
       
    else if (c=='2')//tirar carta da casa de passagem
    {
       if (!sequencia_vazia(passagem1))//so e possivel se a casa nao estiver vazia
           carta=rem(passagem1); 
       else 
       {  
           printf("(Casa vazia!)");  
           return 0;
       }
    }
    else if (c=='3')//tirar carta da casa de passagem
    {
       if (!sequencia_vazia(passagem2))//so e possivel se a casa nao estiver vazia
           carta=rem(passagem2); 
       else 
       {  
           printf("(Casa vazia!)");  
           return 0;
       }
    }
    else if (c=='4')//tirar carta da casa de passagem
    {
       if (!sequencia_vazia(passagem3))//so e possivel se a casa nao estiver vazia
           carta=rem(passagem3); 
       else 
       {  
           printf("(Casa vazia!)");  
           return 0;
       }
    }
    else if (c=='5')//tirar carta da casa de passagem
    {
       if (!sequencia_vazia(passagem4))//so e possivel se a casa nao estiver vazia
           carta=rem(passagem4); 
       else 
       {  
           printf("(Casa vazia!)");  
           return 0;
       }
    }
    else 
    {  
         printf("(invalido!)");  
         return 0;
    }
 
    print_carta(carta);
    por_carta(c);
    return 1;//movimento valido
}

int por_carta(int orig){
    int resultado=0;
    char dest;//casa destino da carta
    int i;
     
    while (resultado==0)
    {
        printf(" -> ");
        dest = getch();
        printf("%c ", dest);
        
        if((orig=='2' || orig=='3' || orig=='4' || orig=='5')//se for de uma casa de passagem 
            &&(dest=='2' || dest=='3' || dest=='4' || dest=='5'))//para outra casa de passagem
            resultado=pass_pass(orig, dest);
    
        else if(dest=='1' && (orig=='0' || orig=='1'))//por no monte; � sempre possivel, 
        {                                             //desde que venha da casa inicial ou do proprio monte     
            push(monte, (void *)carta);
            resultado=1;
        }

        else if (dest=='2' || dest=='3' || dest=='4' || dest=='5')//por numa casa de passagem
        {   
            resultado=por_passagem(dest);
            if (resultado==0)
            {
                recuar_cursor();
                printf(" (invalido!)");
            }
        }
        else if (dest=='6' || dest=='7' || dest=='8' || dest=='9')//por numa casa final
        {
            resultado=por_final(dest);
            if (resultado==0)
            {
                recuar_cursor();
                printf(" (invalido!)");
            }
        }
        else//se nao escolher uma casa, tb da invalido
        {
            recuar_cursor();
            printf(" (invalido!)");
        }
    }
    printf(" (FEITO!)"); 
}

//poe a carta actual numa casa de passagem
int por_passagem(int c){
    
    if(c=='2')
    {
        if (valido_passagem(carta, passagem1)==1)
           add(passagem1, (void *)carta);
        else
            return 0;
    }
    if(c=='3')
    {
        if (valido_passagem(carta, passagem2)==1)
           add(passagem2, (void *)carta);
        else
            return 0;
    }
    if(c=='4')
    {
        if (valido_passagem(carta, passagem3)==1)
           add(passagem3, (void *)carta);
        else
            return 0;
    }
    if(c=='5')
    {
        if (valido_passagem(carta, passagem4)==1)
           add(passagem4, (void *)carta);
        else
            return 0;
    }
    return 1;
}

//poe a carta actual numa casa final
int por_final(int c){
    
    if(c=='6')
    {
        if (valido_final(carta, final1)==1)
           push(final1, (void *)carta);
        else
            return 0;
    }
    if(c=='7')
    {
        if (valido_final(carta, final2)==1)
           push(final2, (void *)carta);
        else
            return 0;
    }
    if(c=='8')
    {
        if (valido_final(carta, final3)==1)
           push(final3, (void *)carta);
        else
            return 0;
    }
    if(c=='9')
    {
        if (valido_final(carta, final4)==1)
           push(final4, (void *)carta);
        else
            return 0;
    }
    return 1;
}

int pass_pass(int orig, int dest){//move cartas de uma casa de passagem para outra
    int i, n_cartas;
    casa_passagem *pass1;//cria 2 apontadores para casas de passagem
    casa_passagem *pass2;
    
    if (orig=='2')
        pass1=passagem1;//que apontam para as casas pedidas 
    if (orig=='3')
        pass1=passagem2;//de origem
    if (orig=='4')
        pass1=passagem3;
    if (orig=='5')
        pass1=passagem4;
    
    if (dest=='2')
        pass2=passagem1;//e de destino
    if (dest=='3')
        pass2=passagem2;
    if (dest=='4')
        pass2=passagem3;
    if (dest=='5')
        pass2=passagem4;
        
    add(pass1, (void *)carta);  //temos de voltar a por a carta que tiramos no inicio da jogada  
    
    if (orig==dest)//se forem a mesma casa, esta feito
        return 1;
        
    n_cartas=valido_pass_pass(pass1, pass2);//verifica se � possivel mover
                                            //e quantas cartas da para mover
    if (n_cartas==0)//se nao for possivel, devolve 0
    {
        recuar_cursor();
        printf(" (invalido!)");
        carta=rem(pass1);//se nao fize a opera�ao, volta a retirar a carta, como estava antes
        return 0;
    }
    else                //se for possivel
        move(pass1, pass2, n_cartas);//move n_cartas
        
            
    //guarda a jogada no registo para detectar repeti�oes
    //1� "move" as jogadas em registo 1 posi�ao para a frente
    //para por a ultima jogada na posi�ao 0
    for (i=5 ; i>0 ; i--)
    {
        registo[0][i]=registo[0][i-1];
        registo[1][i]=registo[1][i-1];
        registo[2][i]=registo[2][i-1];
    }
    registo[0][0]=n_cartas;
    registo[1][0]=orig;
    registo[2][0]=dest;
        
    return 1;    
}

int sair(){
    char c;
    
    limpar_penultima_linha();
    printf("  SAIR: Deseja gravar o jogo?");
    printf("\n  1 - Sim");
    printf("\n  2 - Nao");
    
    while(c!='1' && c!='2')
       c = getch();
    if (c=='1')
        gravar_jogo();
        
    limpar_ultima_linha();
    limpar_penultima_linha();
    printf("  SAIR: Deseja jogar de novo?");
    printf("\n  1 - Sim");
    printf("\n  2 - Nao");
    
    c='0';
    while(c!='1' && c!='2')
       c = getch(); 
    
    limpar_variaveis();   
       
    if (c=='2')
        exit(1);   
    
    limpar_ultima_linha();
    return 1;
}

gravar_jogo(){//grava o jogo em ficheiro
    pilha *p;
    FILE *ficheiro;
    
    p=pilha_nova(MAX_CARTAS_BARALHO);
    ficheiro=fopen("jogo_guardado.txt", "w");
    
    while(inicial->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)pop(inicial));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
    
    while(monte->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)pop(monte));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
    
    while(passagem1->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)rem(passagem1));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
    
    while(passagem2->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)rem(passagem2));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
    
    while(passagem3->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)rem(passagem3));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
    
    while(passagem4->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)rem(passagem4));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
    
    while(final1->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)pop(final1));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
    
    while(final2->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)pop(final2));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
    
    while(final3->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)pop(final3));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
    
    while(final4->size > 0)    //poe todas as cartas da casa na pilha temporaria
        push(p, (void *)pop(final4));
    fprintf( ficheiro, "%d", p->size ) ;//o 1� n� da linha, diz quantas carta ha para ler nessa linha
    while(p->size > 0)  //e grava no ficheiro
        fprintf( ficheiro, " %d", pop(p) ) ;
    fprintf( ficheiro, "\n") ;
        
    pilha_destroi(p);              
    fclose(ficheiro);          
}

int carregar_jogo(){//carrega o ultimo jogo guardado, se nao houver, come�a um novo
    int i, c;
    FILE *ficheiro;
              
    ficheiro=fopen("jogo_guardado.txt", "r");
    
    if (ficheiro == NULL)//se nao houver, come�a um novo
    {
        novo_jogo;
        printf("\nNao ha jogo guardado! Iniciado novo jogo.");
        printf("\nPrima qq tecla para continuar....");
        getch();
        limpar_ultima_linha();
        return 0;
    }
    //iniciar as casas 
    inicial = pilha_nova(MAX_CARTAS_BARALHO);//para a inicial, iniciamos uma pilha vazia em vez de um baralho
    monte = casa_monte_nova();
    passagem1 = casa_passagem_nova();
    passagem2 = casa_passagem_nova();
    passagem3 = casa_passagem_nova();
    passagem4 = casa_passagem_nova();
    final1 = casa_final_nova();
    final2 = casa_final_nova();
    final3 = casa_final_nova();
    final4 = casa_final_nova();
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        push(inicial, (void *)c);        
        i--;  
    }
    fscanf(ficheiro, "\n");//mudar de linha
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        push(monte, (void *)c);        
        i--;  
    }
    fscanf(ficheiro, "\n");//mudar de linha
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        add(passagem1, (void *)c);        
        i--;  
    }
    fscanf(ficheiro, "\n");//mudar de linha
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        add(passagem2, (void *)c);        
        i--;  
    }
    fscanf(ficheiro, "\n");//mudar de linha
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        add(passagem3, (void *)c);        
        i--;  
    }
    fscanf(ficheiro, "\n");//mudar de linha
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        add(passagem4, (void *)c);        
        i--;  
    }
    fscanf(ficheiro, "\n");//mudar de linha
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        push(final1, (void *)c);        
        i--;  
    }
    fscanf(ficheiro, "\n");//mudar de linha
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        push(final2, (void *)c);        
        i--;  
    }
    fscanf(ficheiro, "\n");//mudar de linha
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        push(final3, (void *)c);        
        i--;  
    }
    fscanf(ficheiro, "\n");//mudar de linha
    
    fscanf(ficheiro, "%d", &i);//quantas carta ha para ler na linha
    while(i > 0)
    {
        fscanf(ficheiro, "%d", &c);
        push(final4, (void *)c);        
        i--;  
    }
    
    fclose(ficheiro);          
}

limpar_variaveis(){
      pilha_destroi(inicial); 
      pilha_destroi(monte);           
      sequencia_destroi(passagem1);     
      sequencia_destroi(passagem2);     
      sequencia_destroi(passagem3);     
      sequencia_destroi(passagem4); 
      pilha_destroi(final1);       
      pilha_destroi(final2);       
      pilha_destroi(final3);       
      pilha_destroi(final4);                  
}

int jogar_automatico(){//verifica se ha numa casa de passagem, alguma carta que encaixe numa casa final
    
      if (!sequencia_vazia(passagem1))
      {       
          carta=rem(passagem1);//ve a carta da casa de passagem 1
          if (valido_final(carta, final1)==1)
          {
              push(final1, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final2)==1)
          {
              push(final2, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final3)==1)
          {
              push(final3, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final4)==1)
          {
              push(final4, (void *)carta);
              return 1;
          }
          else 
              add(passagem1, (void *)carta);
      }
           
      if (!sequencia_vazia(passagem2))
      {        
          carta=rem(passagem2);//ve a carta da casa de passagem 2
          if (valido_final(carta, final1)==1)
          {
              push(final1, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final2)==1)
          {
              push(final2, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final3)==1)
          {
              push(final3, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final4)==1)
          {
              push(final4, (void *)carta);
              return 1;
          }
          else 
              add(passagem2, (void *)carta);
      }
          
      if (!sequencia_vazia(passagem3))
      {       
          carta=rem(passagem3);//ve a carta da casa de passagem 3
          if (valido_final(carta, final1)==1)
          {
              push(final1, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final2)==1)
          {
              push(final2, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final3)==1)
          {
              push(final3, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final4)==1)
          {
              push(final4, (void *)carta);
              return 1;
          }
          else 
              add(passagem3, (void *)carta);
      }
            
      if (!sequencia_vazia(passagem4))
      {       
          carta=rem(passagem4);//ve a carta da casa de passagem 4
          if (valido_final(carta, final1)==1)
          {
              push(final1, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final2)==1)
          {
              push(final2, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final3)==1)
          {
              push(final3, (void *)carta);
              return 1;
          }
          else if (valido_final(carta, final4)==1)
          {
              push(final4, (void *)carta);
              return 1;
          }
          else 
              add(passagem4, (void *)carta); 
      }
      
      return 0;      
}

int fim_jogo(){//detecta se o jogo terminou
    char escolha;
    
    if (ganhou()==1)//se o utilizador ganhou
        return 1;
        
    if (bloqueado()==1)//ou se na ha mais jogadas
        return 1;
     
    if (repetidas()==1)//e se as ultimas jogadas foram repetidas
    {                  //pergunta se quer sair ou continuar
        
        limpar_ultima_linha(); 
        limpar_penultima_linha(); 
        printf("  Jogadas repetidas detectadas"); 
        printf("\n  Deseja continuar a jogar?");
        printf("\n  1 - Sim");
        printf("\n  2 - Nao");
		while(escolha!='1' && escolha!='2')
			escolha = getch();
        limpar_ultima_linha(); 
        limpar_penultima_linha();  
        if (escolha=='1')
            return 0; 
        if (escolha=='2')
            return 1;      
    }   
        
    return 0;
}

int ganhou(){//verifica se ganhou o jogo
      int carta_final1;
    int carta_final2;
    int carta_final3;
    int carta_final4;
    char escolha='0';
        
    if (!pilha_vazia(final1) && !pilha_vazia(final2)
     && !pilha_vazia(final3) && !pilha_vazia(final4))  
    {          
        carta_final1=pop(final1);                //tira as cartas para verificar
        push(final1, (void *)carta_final1);
        carta_final2=pop(final2);
        push(final2, (void *)carta_final2);
        carta_final3=pop(final3);
        push(final3, (void *)carta_final3);
        carta_final4=pop(final4);
        push(final4, (void *)carta_final4);
    
        if (numero(carta_final1)=='K' && numero(carta_final2)=='K' //se as casa finais estam completas
         && numero(carta_final3)=='K' && numero(carta_final4)=='K' )//ganhou o jogo
        {
            limpar_ultima_linha(); 
            limpar_penultima_linha(); 
            printf("  PARABENS! - Terminou o Solitario"); 
            printf("\n  Deseja jogar de novo?");
            printf("\n  1 - Sim");
            printf("\n  2 - Nao");
            while(escolha!='1' && escolha!='2')
                escolha = getch(); 
            if (escolha=='2')
                exit(1);
            limpar_ultima_linha(); 
            limpar_penultima_linha(); 
            return 1;
        }
    }
    return 0;  
}

int bloqueado(){//verifica se nao ha mais jogadas possiveis
                //ve se alguma carta do topo de uma casa de passagem ou monte encaixa
                //numa casa de passagemou final
      if(!pilha_vazia(inicial))
          return 0;//se a casa inicial na estiver vazai, ainda ha jogadas
      
      if (!pilha_vazia(monte))
      {       
          carta=pop(monte);//ve a carta do monte
          push(monte, (void *)carta);
          if (valido_final(carta, final1)==1)
              return 0;
          if (valido_final(carta, final2)==1)
              return 0;
          if (valido_final(carta, final3)==1)
              return 0;
          if (valido_final(carta, final4)==1)
              return 0;
          if (valido_passagem(carta, passagem1)==1)
              return 0;
          if (valido_passagem(carta, passagem2)==1)
              return 0;
          if (valido_passagem(carta, passagem3)==1)
              return 0;
          if (valido_passagem(carta, passagem4)==1)
              return 0;
      }  
              
      if (!sequencia_vazia(passagem1))
      {       
          carta=rem(passagem1);//ve a carta da casa de passagem 1
          add(passagem1, (void *)carta);
          if (valido_final(carta, final1)==1)
              return 0;
          if (valido_final(carta, final2)==1)
              return 0;
          if (valido_final(carta, final3)==1)
              return 0;
          if (valido_final(carta, final4)==1)
              return 0;
          if (valido_pass_pass(passagem1, passagem2)>0)
              return 0;
          if (valido_pass_pass(passagem1, passagem3)>0)
              return 0;
          if (valido_pass_pass(passagem1, passagem4)>0)
              return 0;
      }
              
      if (!sequencia_vazia(passagem2))
      {       
          carta=rem(passagem2);//ve a carta da casa de passagem 2
          add(passagem2, (void *)carta);
          if (valido_final(carta, final1)==1)
              return 0;
          if (valido_final(carta, final2)==1)
              return 0;
          if (valido_final(carta, final3)==1)
              return 0;
          if (valido_final(carta, final4)==1)
              return 0;
          if (valido_pass_pass(passagem2, passagem1)>0)
              return 0;
          if (valido_pass_pass(passagem2, passagem3)>0)
              return 0;
          if (valido_pass_pass(passagem2, passagem4)>0)
              return 0;
      }
              
      if (!sequencia_vazia(passagem3))
      {       
          carta=rem(passagem3);//ve a carta da casa de passagem 3
          add(passagem3, (void *)carta);
          if (valido_final(carta, final1)==1)
              return 0;
          if (valido_final(carta, final2)==1)
              return 0;
          if (valido_final(carta, final3)==1)
              return 0;
          if (valido_final(carta, final4)==1)
              return 0;
          if (valido_pass_pass(passagem3, passagem1)>0)
              return 0;
          if (valido_pass_pass(passagem3, passagem2)>0)
              return 0;
          if (valido_pass_pass(passagem3, passagem4)>0)
              return 0;
      }
              
      if (!sequencia_vazia(passagem4))
      {       
          carta=rem(passagem4);//ve a carta da casa de passagem 4
          add(passagem4, (void *)carta);
          if (valido_final(carta, final1)==1)
              return 0;
          if (valido_final(carta, final2)==1)
              return 0;
          if (valido_final(carta, final3)==1)
              return 0;
          if (valido_final(carta, final4)==1)
              return 0;
          if (valido_pass_pass(passagem4, passagem1)>0)
              return 0;
          if (valido_pass_pass(passagem4, passagem2)>0)
              return 0;
          if (valido_pass_pass(passagem4, passagem3)>0)
              return 0;
      }   
      //se estiver bloqueado, pergunta se quer sair ou jogar de novo
    char escolha;
    limpar_ultima_linha(); 
    limpar_penultima_linha(); 
    printf("  JOGO BLOQUEADO - Nao ha mais jogadas possiveis"); 
    printf("\n  Deseja jogar de novo?");
    printf("\n  1 - Sim");
    printf("\n  2 - Nao");
    while(escolha!='1' && escolha!='2')
        escolha = getch(); 
    if (escolha=='2')
        exit(1);
    limpar_ultima_linha(); 
    limpar_penultima_linha();
     
    return 1;
}

int repetidas(){//verifica se nas ultimas 5 jogadas, ha 3 repatidas
    int i, j, repetidas;
    
    for(i=0 ; i<5 ; i++)
    {
        repetidas=1;    
        for(j=0 ; j<5 ; j++)
            if (i!=j)
            {
                if (registo[0][i]==registo[0][j] //se a jogada for igual
                 && registo[1][i]==registo[1][j] 
                 && registo[2][i]==registo[2][j])
                    repetidas++;                 //esta repetida
            }
        if (repetidas>2)   //se achar 3 jogadas iguais
            return 1;
    }
    
    return 0;
}
//*******************************************
//  FUN�OES PARA ESCREVER NO ECRA
//*******************************************
//mostra o menu inicial
int menu(){
     char escolha;
    
     printf("\n  José Alves N 21467"); 
     printf("\n  Daniela Flórido N 21899"); 
     printf("\n\n");  
     printf("\n   ****  ***  *     * *****  ***  ****  *  *** "); 
     printf("\n  *     *   * *     *   *   *   * *   * * *   *"); 
     printf("\n  *     *   * *     *   *   *   * *   * * *   *"); 
     printf("\n   ***  *   * *     *   *   ***** ****  * *   *"); 
     printf("\n      * *   * *     *   *   *   * *   * * *   *"); 
     printf("\n      * *   * *     *   *   *   * *   * * *   *"); 
     printf("\n  ****   ***  ***** *   *   *   * *   * *  *** "); 
     printf("\n"); 
     printf("\n  INSTRUCOES:"); 
     printf("\n  Escolha de 0 a 9 para pegar numa carta"); 
     printf("\n  desse monte, a carta escolhida e mostrada.");  
     printf("\n  De seguida, escolha o monte de destino da carta."); 
     printf("\n  Entre casas de passagem, pode mover varias\n  cartas de uma vez");  
     printf("\n\n"); 
     printf("\n  1 - Iniciar novo jogo"); 
     printf("\n  2 - Carregar jogo guardado"); 
     printf("\n  3 - Sair"); 
    
     while (1)
     {
         escolha = getch();
         if (escolha=='1')
             return 1;
         if (escolha=='2')
             return 2;
         if (escolha=='3')
             exit(1);
     }
}
//mostra o estado actual do jogo
int refresh(){
    
   limpar_ecra();

   printf("-------------------------------------------------\nINICI 0 - (%2d CARTAS)", inicial->size);  
   printf("\n-------------------------------------------------\nMONTE 1 - ");    
   print_monte(monte);
   printf("\n-------------------------------------------------\nPASS  2 - ");    
   print_passagem(passagem1);
   printf("\n-------------------------------------------------\nPASS  3 - ");    
   print_passagem(passagem2);
   printf("\n-------------------------------------------------\nPASS  4 - ");    
   print_passagem(passagem3);
   printf("\n-------------------------------------------------\nPASS  5 - ");    
   print_passagem(passagem4);
   printf("\n-------------------------------------------------\nFINAL 6 - ");    
   print_final(final1);
   printf("\n-------------------------------------------------\nFINAL 7 - ");    
   print_final(final2);
   printf("\n------------------------------------------------- \nFINAL 8 - ");    
   print_final(final3);
   printf("\n-------------------------------------------------\nFINAL 9 - ");    
   print_final(final4);
   printf("\n-------------------------------------------------\n"); 
}

int limpar_ultima_linha(){//limpa as ultimas linhas do ecra
    //colocar o cursor na ultima linha:      
   HANDLE stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
   COORD position;
   position.X = 0;
   position.Y = 22;
   SetConsoleCursorPosition(stdoutHandle, position);
   //limpar a linha:
       printf("                                                 "); 
       printf("\n                                                 ");
       printf("\n                                                 ");      
      
   position.X = 0;
   position.Y = 22;
   SetConsoleCursorPosition(stdoutHandle, position);  //voltar a essa linha
}

int limpar_penultima_linha(){//limpa a linha a seguir as filas de cartas no ecra
    //colocar o cursor na linha:      
   HANDLE stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
   COORD position;
   position.X = 0;
   position.Y = 21;
   SetConsoleCursorPosition(stdoutHandle, position);
   //limpar a linha:
       printf("                                                 ");      
      
   position.X = 0;
   position.Y = 21;
   SetConsoleCursorPosition(stdoutHandle, position);  //voltar a essa linha
}

int recuar_cursor(){
    //colocar o cursor na ultima linha:      
   HANDLE stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
   COORD position;
   position.X = 9;
   position.Y = 22;
   SetConsoleCursorPosition(stdoutHandle, position);
   //limpar a linha:   
   printf("                                                 ");
   SetConsoleCursorPosition(stdoutHandle, position);       
}

int limpar_ecra(){
   int i;
    //colocar o cursor no inicio:      
   HANDLE stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
   COORD position;
   position.X = 0;
   position.Y = 0;
   SetConsoleCursorPosition(stdoutHandle, position);
   //limpar o ecra:
   for (i=0;i<23;i++) 
       printf("                                                 \n");
   
   SetConsoleCursorPosition(stdoutHandle, position);       
}      
