
#include <stdio.h>
#include "pilha.h"

// Cria uma nova pilha 
pilha * pilha_nova(int max){
   pilha *nova = (pilha *) malloc(sizeof(pilha));
// Inicializa os dados 
   nova->topo = NULL;
   nova->max = max;
   nova->size = 0;
   return nova;
}

// Retorna 1 se a pilha esta vazia e 0 caso contrario 
int pilha_vazia(pilha *p){
   if(!p){
      printf("\nPilha_vazia: parametro nulo");
   }
   else if (p->size==0)
        return 1;
   else 
        return 0;
}

// Empilha um dado 
void push(pilha *p, void * dados){
   Elemento *elemento;
   
   if(!p){
      printf("\nPUSH: parametro nulo");
   }   else if(p->size == p->max){
      printf("\nPUSH: pilha cheia");
   }
   else{
   // Cria um novo elemento e coloca no topo da pilha 
      elemento = (Elemento *) malloc(sizeof(Elemento));//vai reservando memoria � medida que adiciona os elementos
      elemento->dados = dados;
      elemento->proximo = p->topo;//proximo aponta para o que estava no topo
      p->topo = elemento;     //o elemento actual passa a ser o "topo"
      p->size++;
   }
}

// remove o topo da pilha. 
void * pop(pilha *p){
   Elemento *novo_topo;
   void * dados;
   if(!p){
      printf("\nPOP: parametro nulo");
   }
   // Se a pilha esta vazia 
   else if(p->size==0)
   {
      printf("\nPOP: pilha vazia");   
   }
   // Se tem algo na pilha 
   else
   {
      novo_topo = p->topo->proximo;
      dados = p->topo->dados;//devolve os dados
      free(p->topo);         //destroi o elemento             
      p->topo = novo_topo;   //coloca o 2� no topo
      p->size--;
      return dados;
   }
}

// Destroi a pilha, e o seu conteudo 
int pilha_destroi(pilha *p){
   if(!p){
      printf("\nPilha_destroi: parametro nulo");
   }
   free(p);
}

