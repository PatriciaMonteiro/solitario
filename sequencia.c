
#include <stdio.h>
#include "sequencia.h"

// Cria uma nova sequencia 
sequencia * sequencia_nova(int max){
   int i;
          
   sequencia *nova = (sequencia *) malloc(sizeof(sequencia));
// Inicializa os dados 
   nova->max = max;
   nova->size = 0;
   for(i = 0; i < max ; i++ )
   {
   // inicializa o vector
      nova->dados[i] = NULL;
   }
   return nova;
}

// Retorna 1 se a sequencia esta vazia e 0 caso contrario 
int sequencia_vazia(sequencia *s){
   if(!s){
      printf("\nSequencia_vazia: parametro nulo");
   }
   else if (s->size==0)
        return 1;
   else 
        return 0;
}

// Adiciona um dado  
void add(sequencia *s, void * dados){
   if(!s){
      printf("\nADD: parametro nulo");
   }   
   else if(s->size == s->max){
      printf("\nADD: sequencia cheia");
   }
   else{
      s->dados[s->size]=dados;//adiciona na 1� pos livre  
      s->size++;
   }
}

// Retorna o ultimo elemento.
void * rem(sequencia *s){
   void * dados;
   
   if(!s){
      printf("\nREM: parametro nulo");
   }   
   else if(s->size == 0){
      printf("\nREM: sequencia vazia");
   }
   else{   
      s->size--;
      dados=s->dados[s->size]; 
      s->dados[s->size] = NULL;//apaga essa posi�ao 
      return dados;
   }
}    
     
//Move n elementos de s1 para s2
void * move(sequencia *s1, sequencia *s2, int n){
     int i;
     
     if(!s1 || !s2){
         printf("\nMOVE: parametro nulo");
     } 
     if (s1->size < n && (s2->max-s2->size) < n)//s1 tem de ter pelo menos n elementos e s2 pelo menos n livres
     {
         printf("\nMOV: impossivel mover");             
     }
     else
     {   
         for(i = 0; i < n ; i++ )//mover n vezes
         {
             s2->dados[s2->size]=s1->dados[s1->size-n+i];//da ultima -n posi�ao de s1 para a ultima de s2
             s2->size++;                                       //que vai crescendo
         }
         s1->size=s1->size-n;//no fim actualiza-se o tamanho de s1
     }
}     
     
// Destroi a sequencia 
int sequencia_destroi(sequencia *s){
    free(s);    
}


