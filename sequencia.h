#ifndef _H_sequencia
#define _H_sequencia
#define MAX_SEQ 20// pode criar sequancias de ate 20 unidades

typedef struct sequencias_{
   void *dados[MAX_SEQ];//pode ser de qq tipo; 
   int max;//tamanho maximo
   int size;//tamanho actual
}sequencia;

// Cria uma nova sequencia 
sequencia * sequencia_nova();

// Retorna 1 se a sequencia esta vazia e 0 caso contrario 
int sequencia_vazia(sequencia *s);

// Adiciona um dado  
void add(sequencia *s, void * dados);

// Retorna o ultimo elemento.
void * rem(sequencia *s);

//Move n elementos de s1 para s2
void * move(sequencia *s1, sequencia *s2, int n);

// Destroi a sequencia 
int sequencia_destroi(sequencia *s);


#endif
