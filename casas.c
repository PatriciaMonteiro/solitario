#include <stdio.h>
#include <windows.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "casas.h"
#include "pilha.h"
#include "sequencia.h"


// Cria uma nova casa_inicial com as cartas em ordem aleatoria
casa_inicial * casa_inicial_nova(){
      int ordenadas[MAX_CARTAS_BARALHO];
      int carta;

      int i,j,x;
      x=0;
      for(i=1; i<5; i++)        //as cartas sao inteiros hexadecimais de 2 algarismos
          for(j=1; j<14; j++, x++)//assim o 1� diz o naipe e o 2� o numero da carta
              ordenadas[x]=i*(0x10)+j;//como ha 13 cartas, os algarismos decimais nao chegavam

      //para a pilha vao por ordem aleatoria
      pilha *inicial;
      inicial = pilha_nova(MAX_CARTAS_BARALHO);
          
      srand(time(NULL));
      j=MAX_CARTAS_BARALHO;//numero de cartas ainda no vector ordenadas
      for(i=0; i<MAX_CARTAS_BARALHO; i++)
      {
          x=rand() % j;//numero entre 0 e j-1
          //printf("%2d %2d %2d %d -> %d\n", i+1, j, x, ordenadas[j-1], ordenadas[x]);
          push(inicial, (void *)ordenadas[x]);//mete a carta na pilha 
          ordenadas[x] = ordenadas[j-1];//copia a ultima para essa posi�ao
          j--;                              //o vector passa a ter menos 1 carta
      }  
         
      return inicial;
}

// Cria uma nova casa_monte vazia
casa_monte * casa_monte_nova(){
           
      casa_monte *monte;
      monte = pilha_nova(MAX_CARTAS_BARALHO);
            
      return monte;
}

// Cria uma nova casa_final vazia
casa_final * casa_final_nova(){
           
      casa_final *final;
      final = pilha_nova(MAX_CARTAS_FINAL);
            
      return final;    
}

// Cria uma nova casa_passagem vazia
casa_passagem * casa_passagem_nova(){
           
      casa_passagem *passagem;
      passagem = sequencia_nova(MAX_CARTAS_PASSAGEM);
            
      return passagem;    
}

//***************************************************
//       FUN�OES PARA MOVIMENTAR CARTAS
//***************************************************

//move cartas de uma casa de passagem para outra casa de passagem
void passagem_passagem(casa_passagem *f1, casa_passagem *f2){
     int carta;
     int x;
     x=valido_pass_pass(f1,f2);
     if (x!=0)
        move(f1, f2, x);
}

//determinar o naipe de uma carta
char naipe(int carta){
   int dezenas;
   
   dezenas = carta/0x10; //o algarismo das "dezenas" do numero hexadecimal diz o naipe das cartas
   
   if (dezenas == 1)
      return 3;     //simbolo ascii de copas
   if (dezenas == 2)
      return 4;     //simbolo ascii de ouros
   if (dezenas == 3)
      return 5;     //simbolo ascii de paus
   if (dezenas == 4)
      return 6;     //simbolo ascii de espadas
}

//determinar o numero de uma carta
char numero(int carta){
   int unidades;
   
   unidades = carta%0x10; //o algarismo das unidades do numero hexadecimal diz o numero das cartas
   
   if (unidades==1)
      return 'A';
   if (unidades==2)
      return '2';
   if (unidades==3)
      return '3';
   if (unidades==4)
      return '4';
   if (unidades==5)
      return '5';
   if (unidades==6)
      return '6';
   if (unidades==7)
      return '7';
   if (unidades==8)
      return '8';
   if (unidades==9)
      return '9';
   if (unidades==0xA)
      return '0';
   if (unidades==0xB)
      return 'J';
   if (unidades==0xC)
      return 'Q';
   if (unidades==0xD)
      return 'K';
}

//determina se uma carta "encaixa" numa casa final; devolve 1 se for possivel e 0 se nao for
int valido_final(int carta, casa_final *f){
    int carta_casa;

    if (f->size==0) //se estivar vazia pode-se sempre por la um AS
    {
        if (numero(carta)=='A')
           return 1;
        else
            return 0;
    }
	
	//carta_casa=f->topo->dados;
    carta_casa = pop(f);//se nao estiver vazia, temos de ver qual � a ultima carta
    push(f, (void *)carta_casa);//vota a por a carta que tirou
         
    if ((carta%0x10)==(carta_casa%0x10)+1) //se for a carta imediatamente a seguir � que esta na casa, � valido
        if (naipe(carta)==naipe(carta_casa))//e se forem do mesmo naipe
           return 1;   
           
    return 0;
}

//determina se uma carta "encaixa" numa casa de passagem; devolve 1 se for possivel e 0 se nao for
int valido_passagem(int carta, casa_passagem *p){
    int carta_casa;

    if (p->size==0) //se estivar vazia pode-se sempre por
           return 1;
    
	//carta_casa=p->dados[p->size-1];
    carta_casa = rem(p);//se nao estiver vazia, temos de ver qual � a ultima carta
    add(p, (void *)carta_casa);//vota a por a carta que tirou
     
    if ((carta%0x10)==(carta_casa%0x10)-1) //se for a carta imediatamente antes(de qq naipe) da que esta na casa, � valido
        return 1;                          //para isso vemos o algarismo das unidades
    else
        return 0;
}

//determina se � possivel passar cartas de uma sequencia para outra
//devolve o numero de cartas que pode passar, ou 0 se nao for possivel
int valido_pass_pass(casa_passagem *p1, casa_passagem *p2){
    int carta1;    
    int carta2;
    int i;
    
    if (p2->size==0) //se o destino estiver vazio
       return p1->size;  //podemos mover todas
    
    carta2=p2->dados[p2->size-1];//vamos comparar as cartas da casa de origem com a ultima carta da casa de destino
    for(i=0; i<p1->size; i++)
    {
        carta1=p1->dados[i];             
        if ((carta1%0x10)==(carta2%0x10)-1)//procuramos uma carta que encaixe na ultima carta da casa de destino
            return (p1->size)-i;//podemos mudar a carta que encaixa e as seguintes
    }

       return 0;
}
//***************************************************
//       MOSTRAR O ESTADO CORRENTE DO JOGO
//***************************************************
//faz print de uma carta
//converte o numero inteiro para uma representa�ao de 2 caracteres da carta
//por exemplo:
//AS de copas:   11 -> AC 
//rei de ouros: 2D -> RO
void print_carta(int carta){
     char c1;
     char c2;
         
     c1=naipe(carta);
     c2=numero(carta);
     
     if (c2=='0') 
         printf("1%c%c", c2, c1);
     else
         printf(" %c%c", c2, c1);     
}

//faz print das cartas de uma casa final
//para isso vamos tirar todas as cartas de uma casa e voltar a mete-las no fim
void print_final(casa_final *p){
     
     int carta;
     pilha *q;
     q = pilha_nova(13);
     
     while (p->size!=0){//tirar as cartas
           carta=pop(p);
           push(q, (void *)carta);  
     }
      
     while (q->size!=0){//voltar a por e fazer print
           carta=pop(q);
           push(p, (void *)carta);
           print_carta(carta);  
     }
}
//faz print das 13 ultimas cartas de uma casa monte
//para isso vamos tirar todas as cartas de uma casa e voltar a mete-las no fim
void print_monte(casa_monte *m){
     
     int carta,i;
     pilha *q;
     q = pilha_nova(13);
     
     for(i=0;i<13;i++)
         if(m->size!=0)
         {//tirar 3 cartas
            carta=pop(m);
            push(q, (void *)carta);  
         }
      
     for(i=0;i<13;i++)
         if(q->size!=0)     
         {//voltar a por e fazer print
            carta=pop(q);
            push(m, (void *)carta);
            print_carta(carta);  
         }
}
//faz print das cartas de uma casa de passagem
//nas sequencias basta percorrer o vector
void print_passagem(casa_passagem *p){
     
     int i;
        
     for(i=0;i<p->size;i++)
           print_carta(p->dados[i]);  
}

//*********************************************************
//*********************************************************
//*********************************************************
//*********************************************************
//*********************************************************
void test(){
   casa_inicial *inicial;
   casa_final *final;
   casa_monte *monte;  
   casa_passagem *passagem;
   int carta;
     
   inicial = casa_inicial_nova();
   final = casa_final_nova();
   monte = casa_monte_nova();
   passagem = casa_passagem_nova();
   
   carta=pop(inicial);
   push(final, (void *)carta);
   push(monte, (void *)carta);
   add(passagem, (void *)carta);    
     
     
   print_final(final); 
   print_monte(monte);  
   print_passagem(passagem); 
   
   
   
   
     
     
  
     
     
     }


